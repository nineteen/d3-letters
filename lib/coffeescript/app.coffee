xx = (t) -> console.log t

# settings
GAP    = 20

margin =
  left: 100
  right: 100
  bottom: 50
  top: 50

size =
  width: innerWidth
  height: innerHeight * .7

$window = $(window)
$body = $('body')
$textarea = $('#textarea')

class Svg
  constructor: ->
    @rawData = ""
    @svg = d3.select('body').append('svg')
    @makeXaxis()
    @makeYaxis()
    @setSize()
    @saturationScale = d3.scale.linear().range([0, 70])
    @update() if @rawData
  update: ->
    @rawData = @rawData.replace(/\W|\d/g, '').toLowerCase()
    @groupedData = d3.nest().key((d) -> d).map(@rawData.split('').sort())
    @data = d3.entries(@groupedData)
    # tip: nest, key, map, entries

    @updateXaxis()
    @updateYaxis()
    @saturationScale.domain([0, d3.max(@data, (d) -> d.value.length)])

    selection = @svg.selectAll('rect').data(@data)
    @render(selection)
  makeXaxis: ->
    @xScale = d3.scale.ordinal()
    @xAxis = @svg.append('g')
  updateXaxis: ->
    chars = d3.keys(@groupedData)
    # tip: d3.keys
    @xScale.domain(chars).rangeBands([margin.left, size.width - margin.right])
    @xAxisFn = d3.svg.axis().scale(@xScale).orient('bottom')
    @xAxis.call(@xAxisFn)
  makeYaxis: ->
    @yScale = d3.scale.linear()
    @yAxis = @svg.append('g')
  updateYaxis: ->
    @yScale.domain([0, d3.max(@data, (d) -> d.value.length)]).range([size.height - margin.bottom, margin.top])
    @yAxisFn = d3.svg.axis().scale(@yScale).orient('left').tickFormat(d3.format('d'))
    # tip: orient and tickformat
    @yAxis.call(@yAxisFn)

  render: (selection) ->
    barWidth = (size.width - margin.left - margin.right) / @data.length - GAP
    barWidth = barWidth > 0 && barWidth || 5

    getHeight = (d) =>
      size.height - @yScale(d.value.length) - margin.bottom
    getTransform = (d) =>
      x = @xScale(d.key)
      y = @yScale(d.value.length)
      "translate(#{x + GAP / 2}, #{y})"
    getColor = (d) =>
      saturation = @saturationScale(d.value.length)
      "hsl(1, #{saturation}, 50)"

    selection
      .transition()
      .attr('width', barWidth)
      .attr('height', getHeight)
      .attr('transform', getTransform)
      .attr('fill', getColor)

    selection.enter().append('rect')
      .transition()
      .attr('width', barWidth)
      .attr('height', getHeight)
      .attr('transform', getTransform)
      .attr('fill', getColor)

    selection.exit()
      .transition()
      .attr('width', 0)
      .remove()
  setSize: ->
    size =
      width: innerWidth
      height: innerHeight * .7
    @svg.attr('width', size.width).attr('height', size.height)
    @xAxis.attr('transform', "translate(0, #{size.height - margin.bottom})")
    @yAxis.attr('transform', "translate(#{margin.left - 40}, 0)")

svg = new Svg()

$window.on 'resize', ->
  svg.setSize()
  svg.update()

$textarea.on 'keyup', (e) ->
  svg.rawData = e.currentTarget.value
  svg.update()
$textarea.trigger 'keyup'
