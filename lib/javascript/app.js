var $body, $textarea, $window, GAP, Svg, margin, size, svg, xx;

xx = function(t) {
  return console.log(t);
};

GAP = 20;

margin = {
  left: 100,
  right: 100,
  bottom: 50,
  top: 50
};

size = {
  width: innerWidth,
  height: innerHeight * .7
};

$window = $(window);

$body = $('body');

$textarea = $('#textarea');

Svg = (function() {
  function Svg() {
    this.rawData = "";
    this.svg = d3.select('body').append('svg');
    this.makeXaxis();
    this.makeYaxis();
    this.setSize();
    this.saturationScale = d3.scale.linear().range([0, 70]);
    if (this.rawData) {
      this.update();
    }
  }

  Svg.prototype.update = function() {
    var selection;
    this.rawData = this.rawData.replace(/\W|\d/g, '').toLowerCase();
    this.groupedData = d3.nest().key(function(d) {
      return d;
    }).map(this.rawData.split('').sort());
    this.data = d3.entries(this.groupedData);
    this.updateXaxis();
    this.updateYaxis();
    this.saturationScale.domain([
      0, d3.max(this.data, function(d) {
        return d.value.length;
      })
    ]);
    selection = this.svg.selectAll('rect').data(this.data);
    return this.render(selection);
  };

  Svg.prototype.makeXaxis = function() {
    this.xScale = d3.scale.ordinal();
    return this.xAxis = this.svg.append('g');
  };

  Svg.prototype.updateXaxis = function() {
    var chars;
    chars = d3.keys(this.groupedData);
    this.xScale.domain(chars).rangeBands([margin.left, size.width - margin.right]);
    this.xAxisFn = d3.svg.axis().scale(this.xScale).orient('bottom');
    return this.xAxis.call(this.xAxisFn);
  };

  Svg.prototype.makeYaxis = function() {
    this.yScale = d3.scale.linear();
    return this.yAxis = this.svg.append('g');
  };

  Svg.prototype.updateYaxis = function() {
    this.yScale.domain([
      0, d3.max(this.data, function(d) {
        return d.value.length;
      })
    ]).range([size.height - margin.bottom, margin.top]);
    this.yAxisFn = d3.svg.axis().scale(this.yScale).orient('left').tickFormat(d3.format('d'));
    return this.yAxis.call(this.yAxisFn);
  };

  Svg.prototype.render = function(selection) {
    var barWidth, getColor, getHeight, getTransform;
    barWidth = (size.width - margin.left - margin.right) / this.data.length - GAP;
    barWidth = barWidth > 0 && barWidth || 5;
    getHeight = (function(_this) {
      return function(d) {
        return size.height - _this.yScale(d.value.length) - margin.bottom;
      };
    })(this);
    getTransform = (function(_this) {
      return function(d) {
        var x, y;
        x = _this.xScale(d.key);
        y = _this.yScale(d.value.length);
        return "translate(" + (x + GAP / 2) + ", " + y + ")";
      };
    })(this);
    getColor = (function(_this) {
      return function(d) {
        var saturation;
        saturation = _this.saturationScale(d.value.length);
        return "hsl(1, " + saturation + ", 50)";
      };
    })(this);
    selection.transition().attr('width', barWidth).attr('height', getHeight).attr('transform', getTransform).attr('fill', getColor);
    selection.enter().append('rect').transition().attr('width', barWidth).attr('height', getHeight).attr('transform', getTransform).attr('fill', getColor);
    return selection.exit().transition().attr('width', 0).remove();
  };

  Svg.prototype.setSize = function() {
    size = {
      width: innerWidth,
      height: innerHeight * .7
    };
    this.svg.attr('width', size.width).attr('height', size.height);
    this.xAxis.attr('transform', "translate(0, " + (size.height - margin.bottom) + ")");
    return this.yAxis.attr('transform', "translate(" + (margin.left - 40) + ", 0)");
  };

  return Svg;

})();

svg = new Svg();

$window.on('resize', function() {
  svg.setSize();
  return svg.update();
});

$textarea.on('keyup', function(e) {
  svg.rawData = e.currentTarget.value;
  return svg.update();
});

$textarea.trigger('keyup');
